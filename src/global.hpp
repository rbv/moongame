#ifndef COMMON_HPP
#define COMMON_HPP

#include <SDL.h>
#include "types.hpp"
#include "object.hpp"
#include "nfont/NFont.h"
#include <list>

using namespace std;

struct GlobalState {
    SDL_Window* window;
    SDL_Renderer* renderer;
    XY screenSz;

    // Number of seconds this frame
    double stepSec;

    double cor;
    double friction;

    double totalEnergy;

    bool paused;
    bool gravityEnabled;

    // The centre of the viewport, measured in units
    XYd camera;

    // pixels per distance unit
    double scale;

    // resources, to be moved later
    SDL_Texture *stars;
    SDL_Texture *moon;
    NFont guifont;
    NFont chatfont;


    list<Object> objects;
    
    GlobalState() {
        camera = XYd{{0.0}, {0.0}};
        paused = false;
        gravityEnabled = true;
        scale = 1.0;
    }

};

extern GlobalState gstate;

#define objectsBegin gstate.objects.begin
#define objectsEnd gstate.objects.end

const char nl = '\n';


#endif
