/* Floating text, as well as any heads-up-display or other GUI stuff
 */

#include "globals.hpp"
#include <string>
#include <list>

struct Message {
    XYd pos;  // Top left
    Object *speaker;  // NULL if not associated with an object

    std::string text;
    SDL_Texture *tex;

    // In seconds
    double timeToDisplay;
    double timeLeft;
};

class Overlay {
    std::list<Message> messages;

public:
    void displayMessages();
    void updateMessages();
};

extern Overlay gui;

void Overlay::displayMessages() {
    SDL_Rect dest = {0, 0, 0, 0};    

    // display messages
    deque<CScreenMessage *>::iterator i;
    
    updateMessages();
    
    dest.x = mapsection.x + 10;
    dest.y = mapsection.y + 10;
    
    // input field drawn as a message
    if (textHandler) {
        textHandler->draw(dest);
        dest.y += textHandler->getHeight();
    }
    
    for (i = messages.begin(); i < messages.end(); i++) {
        (*i)->draw(dest);
        dest.y += (*i)->getHeight();
    }
    
    /*
    for (i = 0; i < messages.size(); i++) {
        messages[i]->draw(dest);
        dest.y += messages[i]->getHeight();
    }
    */
    
    // display alerts
    list<CTextField *>::iterator j = alerts.begin();
    dest.x = mapsection.x + 20;
    dest.y = mapsection.h - 40;
    
    while (j != alerts.end()) {
        (*j)->draw(dest);
        dest.y -= (*j)->getHeight();
        ++j;
    }
}

void Overlay::addMessage(const char *key, const char *text, SDL_Color &colour) {
    cout << "adding message " << text << endl;
    CScreenMessage *newmess = new CScreenMessage(key, text, colour);
    messages.push_front(newmess);
    cout << "finished adding message" << endl;
}

CTextField *Overlay::addAlert(const char *text, SDL_Color &colour) {
    // arbitrarily set field width to half map?
    CTextField *temp = new CTextField(text, colour, mapsection.w / 2);
    alerts.push_back(temp);
    return temp;
}

void Overlay::removeAlert(CTextField *textfragment) {
    alerts.remove(textfragment);
}

void Overlay::updateMessages() {
    CScreenMessage *temp;
    while (messages.size() && (temp = messages.back())->birthtime + 6000 < SDL_GetTicks()) {
        messages.pop_back();
        delete temp;
    }
}
