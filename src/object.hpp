
#ifndef OBJECT_HPP
#define OBJECT_HPP

#include "types.hpp"
#include <SDL.h>
#include <deque>


class Object {
//protected:
public:
    int id;  // debugging convenience

    XYd pos;
    double radius;
    XYd vel;
    double mass;  // if zero, force is unused
    XYd impulse;

    // The following are only used during physics steps

    XYd force;

    double stepRemaining;  // in seconds

    //double minCollisionTime;  // The earliest collision time found so far
    // (If a collision is found earlier, could undo earlier collision; if later could preserve it)

    int collisions;  // Number of collisions that occurred this time step

    // End physics

    SDL_Texture *texture;

    std::deque<XYd> trail;

public:

    Object(XYd _pos, double _radius, int _id = -1)
        : id(_id), pos(_pos), radius(_radius), mass(0.0), texture(NULL) {}

    void physicsStep1();
    void physicsStep2();
    void collisionCheck(Object &rhs);
    void updateAnimations();
};

void physicsStep();
void updateObjectAnimations();
void conservedQuantities();

#endif
