#include "stdio.h"
#include "math.h"
#include "global.hpp"
#include <vector>

using namespace std;

GlobalState gstate;

const double maxPhysicsTimeStep = 0.004;


void SDLerr(const char *msg, ...) {
    static char buf[2048];
    va_list lst;
    va_start(lst, msg);
    vsnprintf(buf, 2048, msg, lst);
    va_end(lst);
    fprintf(stderr, "%s; SDL error: %s\n", buf, SDL_GetError());
}

SDL_Surface *createSurface(int w, int h) {
    return SDL_CreateRGBSurface(0, w, h, 32, 0xff000000, 0xff0000, 0xff00, 0xff);
}

SDL_Surface *loadSurface(const char *name, bool keyed = false) {
    char buf[256];
    snprintf(buf, 256, "../res/%s", name);
    SDL_Surface *ret = SDL_LoadBMP(buf);
    if (!ret) {
        SDLerr("Could not load %s", buf);
        return NULL;
    }
    if (keyed) {
        int col = SDL_MapRGB(ret->format , 255, 0, 255);
        SDL_SetColorKey(ret, true, col);
    }
    return ret;
}

SDL_Texture *loadTexture(const char *name, bool keyed = false) {
    SDL_Surface *tempsurf = loadSurface(name, keyed);
    if (!tempsurf) {
        return NULL;
    }
    SDL_Texture *ret = SDL_CreateTextureFromSurface(gstate.renderer, tempsurf);
    SDL_FreeSurface(tempsurf);
    if (!ret)
        SDLerr("Could not create texture from %s", name);
    return ret;
}

SDL_Texture *toTexture(SDL_Surface *surf) {
    SDL_Texture *ret = SDL_CreateTextureFromSurface(gstate.renderer, surf);
    SDL_FreeSurface(surf);
    if (!ret)
        SDLerr("Could not create texture from surface");
    return ret;
}




class Sprite {
    SDL_Texture *tex;
    SDL_Rect bounds;

public:
    Sprite() : tex(NULL) {}
    Sprite(SDL_Texture *texture);

    ~Sprite() { SDL_DestroyTexture(tex); }

    void setBounds(const SDL_Rect &rect) { bounds = rect; }

    // Position on screen in pixels
    void drawAt(XY pos);
    void drawAt(XYd pos) { drawAt(XY(pos)); }
};

Sprite::Sprite(SDL_Texture *texture) {
    tex = texture;
    bounds = SDL_Rect{0, 0, 0, 0};
    if (SDL_QueryTexture(tex, NULL, NULL, &bounds.w, &bounds.h))
        SDLerr("Couldn't query texture");
}

void Sprite::drawAt(XY pos) {
    if (!tex)
        return;
    SDL_Rect destrect{int(pos.x), int(pos.y), 0, 0};
    if (SDL_QueryTexture(tex, NULL, NULL, &destrect.w, &destrect.h))
        SDLerr("Couldn't query texture");
    else {
        SDL_RenderCopy(gstate.renderer, tex, NULL, &destrect);
    }
}


/************************************* Text ***********************************/


char *formatText(const char *format, ...) {
    static char retbuf[2048];
    va_list lst;
    va_start(lst, format);
    vsnprintf(retbuf, 2048, format, lst);
    va_end(lst);
    return retbuf;
}


// Draw a string. If size is not NULL, fill with size of output
SDL_Texture *_renderText(NFont &font, int width, XY *size, const char *text) {
    int height = font.getColumnHeight(width, text);

    SDL_Texture *tex = NULL;
    SDL_Surface *textsurf = createSurface(width, height);
    SDL_Rect rect = font.drawColumn(textsurf, 0, 0, width, text);
    tex = toTexture(textsurf);
    if (size)
        *size = XY(rect.w, rect.h);
    return tex;
}

Sprite *renderText(NFont &font, int width, const char *text) {
    XY size;
    SDL_Texture *texture = _renderText(font, width, &size, text);
    Sprite *ret;
    ret = new Sprite(texture);
    ret->setBounds(SDL_Rect{0, 0, size.w, size.h});
    return ret;
}


/************************************ Drawing *********************************/


// Return the largest non-positive double congruent to x modulo y
double fmodneg(double x, double y) {
    double ret = fmod(x, y);
    if (ret > 0.0)
        ret -= y;
    return ret;
}

// f(-inf) = -1    * max
// f(  -1) = -0.46 * max
// f(   0) = 0     * max
// f(   1) = 0.46  * max
// f(   2) = 0.76  * max
// f( inf) = 1     * max
double logistic(double x, double max = 1.0) {
    return max * (2. / (1 + exp(-x)) - 1);
}

void drawStars() {
    // Parallax:
    // Working backwards from the scale, calculate height of camera. Say the background is a fixed
    // distance behind the playing field. From that find the background scale factor
    //double starsScale = 1 / logistic(1 / gstate.scale + 1, 5);// * gstate.scale;

    // Enforce the ratio starsScale/gstate.scale to be in [0,0.7]
    double starsScale = (logistic(1 / gstate.scale, 0.7)) * gstate.scale;

    // Pixel position of topleft viewport corner
    XYd starsOffset = starsScale * (gstate.camera) - XYd(gstate.screenSz) * 0.5;

    XYd imageSz{512, 512};
    imageSz *= 8 * starsScale; // zoom in

    SDL_Rect destrect = {0, 0, int(imageSz.w), int(imageSz.h)};

    for (double x = fmodneg(-starsOffset.x, imageSz.w); x < gstate.screenSz.w; x += imageSz.w) {
        for (double y = fmodneg(-starsOffset.y, imageSz.h); y < gstate.screenSz.h; y += imageSz.h) {
            destrect.x = x;
            destrect.y = y;
            SDL_RenderCopy(gstate.renderer, gstate.stars, NULL, &destrect);
        }
    }
}

void drawTrails() {
    // Pixel position of topleft viewport corner
    XYd cameraOffset = gstate.scale * gstate.camera - XYd(gstate.screenSz) * 0.5;

    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        vector<SDL_Point> trail;
        for (auto trailIt = it->trail.begin(); trailIt != it->trail.end(); ++trailIt) {
            trail.push_back(SDL_Point(gstate.scale * *trailIt - cameraOffset));
        }

        SDL_SetRenderDrawColor(gstate.renderer, 0, 0, 255, 255);
        SDL_RenderDrawLines(gstate.renderer, &trail[0], trail.size());
    }
}

void drawObjects() {
    // Pixel position of topleft viewport corner
    XYd cameraOffset = gstate.scale * gstate.camera - XYd(gstate.screenSz) * 0.5;

    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        //if (it->texture) {
        XYd screenPos = gstate.scale * it->pos - cameraOffset;
        int radius = gstate.scale * it->radius;
        SDL_Rect destrect = {int(screenPos.x - radius), int(screenPos.y - radius), 2 * radius, 2 * radius};
        SDL_RenderCopy(gstate.renderer, gstate.moon, NULL, &destrect);
        //}
    }
}

/*************************************  Logic *********************************/


void calcFPS(double frameSec, Sprite *&fpsText) {
    static double fpsIntervalLen = 0.0;
    static int fpsIntervalFrames = 0;

    fpsIntervalFrames++;
    if ( (fpsIntervalLen += frameSec) >= 1.0 ) {
        printf("fps: %.1f\n", fpsIntervalFrames / fpsIntervalLen);
        delete fpsText;
        fpsText = renderText(gstate.guifont, 300, formatText("fps: %.1f\n", fpsIntervalFrames / fpsIntervalLen));

        fpsIntervalLen = 0.0;
        fpsIntervalFrames = 0;
    }
}


void scenario1() {
    gstate.objects.clear();

    Object moon({0, 200}, 50);
    moon.vel = XYd(0, 0);
    moon.mass = 1e22;
    gstate.objects.push_back(moon);

    Object moon2({800, 220}, 10);
    moon2.vel = XYd(0, 30);
    moon2.mass = 1e20;
    gstate.objects.push_back(moon2);

    gstate.friction = 0.01;
    gstate.cor = 0.6;
}

void scenario2() {
    gstate.objects.clear();

    Object moon({0, 200}, 50);
    moon.vel = XYd(0, 0);
    moon.mass = 1e22;
    gstate.objects.push_back(moon);

    Object moon2({800, 220}, 10);
    moon2.vel = XYd(0, 50);
    moon2.mass = 1e20;
    gstate.objects.push_back(moon2);

    gstate.friction = 0.0;
    gstate.cor = 0.9;
}

void scenario3() {
    gstate.objects.clear();

    Object moon({300, 200}, 45);
    moon.vel = XYd(-250, 0);

    moon.mass = 7.35e22;   // Actual mass of the Moon
    gstate.objects.push_back(moon);
    moon.pos = {500, 500};
    moon.vel = XYd(250, -200);
    gstate.objects.push_back(moon);
    moon.pos = {700, 700};
    moon.mass = 9.4e20;  // Mass of Ceres
    moon.radius = 20;
    gstate.objects.push_back(moon);

    gstate.friction = 0.0;
    gstate.cor = 1.0;
}


void scenario4() {
    gstate.objects.clear();

    Object moon({300, 200}, 45);
    moon.vel = XYd(0, 0);

    moon.mass = 7.35e22;   // Actual mass of the Moon
    gstate.objects.push_back(moon);

    for (int i = 0; i < 10; i++) {
        moon.pos = {double(300 + i * 40), double(400 + i * 60)};
        moon.vel = XYd(250, -200);
        moon.mass = 0;
        moon.radius = 20;
        gstate.objects.push_back(moon);
    }
    gstate.friction = 0.0;
    gstate.cor = 1.0;
}


// Figure 8 3 body solution
void scenario5() {
    gstate.objects.clear();
    double p1 = 0.347111 * 100, p2 = 0.532728 * 100;

    Object moon({0, 0}, 5);
    moon.mass = 5e20;

    moon.pos = {-100, 0};
    moon.vel = XYd(p1, p2);
    gstate.objects.push_back(moon);
    moon.pos = {100, 0};
    moon.vel = XYd(p1, p2);
    gstate.objects.push_back(moon);
    moon.pos = {0, 0};
    moon.vel = XYd(-2 * p1, -2 * p2);
    gstate.objects.push_back(moon);
}


void mainloop() {
    int numkeys;
    uint8_t* keystate = SDL_GetKeyboardState(&numkeys);
    if (!keystate) {
        fprintf(stderr, "no keyboard state\n");
        return;
    }

    uint32_t lastticks = SDL_GetTicks();


    gstate.chatfont.load(loadSurface("GreenVenus26.bmp"));
    gstate.guifont.load(loadSurface("LtBlueTwCen18Border.bmp"));//"BlueAgency20.bmp"));

    //Sprite *text1 = renderText(gstate.guifont, 300, "Hello there!\naaaaaa aaaaa aaa aa a aaaa aaaaa aaaa aaaff fffaaaa");
    //Sprite *text2 = renderText(gstate.chatfont, 300, "Texting, etc");
    Sprite *fpsText = new Sprite();
    Sprite *energyText = new Sprite();

    while (1) {
        uint32_t ticks = SDL_GetTicks();
        double frameSec = (ticks - lastticks) * 0.001;
        lastticks = ticks;

        calcFPS(frameSec, fpsText);

        gstate.stepSec = min(frameSec, 0.2);

        double scaleMult = 1.0;

        // event handling
        SDL_Event e;
        if (SDL_PollEvent(&e)) {

            if (e.type == SDL_QUIT)
                break;

            else if (e.type == SDL_KEYUP) {
                if (e.key.keysym.sym == SDLK_ESCAPE)
                    break;
                else if (e.key.keysym.sym == SDLK_F4 && (e.key.keysym.mod & KMOD_ALT))
                    break;
                else if (e.key.keysym.sym == SDLK_p)
                    gstate.paused ^= true;
                else if (e.key.keysym.sym == SDLK_g)
                    gstate.gravityEnabled ^= true;
                else if (e.key.keysym.sym == SDLK_1)
                    scenario1();
                else if (e.key.keysym.sym == SDLK_2)
                    scenario2();
                else if (e.key.keysym.sym == SDLK_3)
                    scenario3();
                else if (e.key.keysym.sym == SDLK_4)
                    scenario4();
                else if (e.key.keysym.sym == SDLK_5)
                    scenario5();
            }
            else if (e.type == SDL_MOUSEWHEEL) {
                //scaleMult = pow(1.1, e.wheel.y / 60.);
                if (e.wheel.y > 0) scaleMult = 1.2;
                if (e.wheel.y < 0) scaleMult = 1 / 1.2;
                printf("wheel %d = %f\n", e.wheel.y, scaleMult);
            }
        }

        double camSpeed = gstate.stepSec * 1000 / gstate.scale;
        if (keystate[SDL_SCANCODE_DOWN])  gstate.camera.y += camSpeed;
        if (keystate[SDL_SCANCODE_UP])    gstate.camera.y -= camSpeed;
        if (keystate[SDL_SCANCODE_LEFT])  gstate.camera.x -= camSpeed;
        if (keystate[SDL_SCANCODE_RIGHT]) gstate.camera.x += camSpeed;
        //printf("%f %f\n", gstate.camera.x, gstate.camera.y);

        if (keystate[SDL_SCANCODE_KP_PLUS])  scaleMult = 1.02;
        if (keystate[SDL_SCANCODE_KP_MINUS]) scaleMult = 1 / 1.02;
        if (keystate[SDL_SCANCODE_EQUALS])  scaleMult = 1.02;   // PLUS
        if (keystate[SDL_SCANCODE_MINUS]) scaleMult = 1 / 1.02;
        gstate.scale *= scaleMult;

        if (!gstate.paused) {
            // Physics
            double temp = gstate.stepSec;
            int steps = gstate.stepSec / maxPhysicsTimeStep + 0.9;
            if (steps) {
                gstate.stepSec /= steps;
                for (int i = steps; i; i--) {
                    physicsStep();
                }
            }
            gstate.stepSec = temp;

            conservedQuantities();
            delete energyText;
            energyText = renderText(gstate.guifont, 300, formatText("System energy: %.5g", gstate.totalEnergy));

            updateObjectAnimations();
        }

        // clear the screen
        SDL_SetRenderDrawColor(gstate.renderer, 0, 0, 0, 255);
        SDL_RenderClear(gstate.renderer);

        drawStars();
        drawTrails();
        drawObjects();

        //text1->drawAt(XY{0, 0});
        //text2->drawAt(XY{0, 300});

        fpsText->drawAt(XY{0, gstate.screenSz.h - 20});
        energyText->drawAt(XY{1000, gstate.screenSz.h - 20});

        SDL_RenderPresent(gstate.renderer);

        //FIXME: Should only wait if vsync disabled
        SDL_Delay(5);
    }
}

int main(int argc, char* argv[])
{

    // Initialize SDL.
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        return 1;

#ifdef _WIN32
    freopen("stdout.txt", "w", stdout);
    freopen("stdout.txt", "w", stderr);
#endif

    gstate.screenSz = XY{1275, 920};

    // Create the window where we will draw.
    gstate.window = SDL_CreateWindow("La Lune",
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED, //SDL_WINDOWPOS_CENTERED,
                              gstate.screenSz.w, gstate.screenSz.h,
                              SDL_WINDOW_SHOWN | SDL_WINDOW_MAXIMIZED);

    // We must call SDL_CreateRenderer in order for draw calls to affect this window.
    gstate.renderer = SDL_CreateRenderer(gstate.window, -1, SDL_RENDERER_PRESENTVSYNC);

    gstate.stars = loadTexture("7701.bmp");
    gstate.moon = loadTexture("moon1.bmp", true);

    scenario1();

    gstate.camera = {500, 500};

    mainloop();

    SDL_Quit();
    return 0;
}
