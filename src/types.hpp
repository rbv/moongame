#ifndef TYPES_HPP
#define TYPES_HPP

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iterator>
#include <SDL_rect.h>

template<typename iter_t>
inline iter_t incPtr(iter_t it, int amount = 1) {
    iter_t ret = it;
    std::advance(ret, amount);
    return ret;
}

struct XYd;

struct XY {
    union { int x, w; };
    union { int y, h; };

    XY() : x(0), y(0) {}
    XY(int _x, int _y) : x(_x), y(_y) {}
    XY(XYd pt);
};

struct XYd {
    union { double x, w; };
    union { double y, h; };

    XYd() : x(0.0), y(0.0) {}
    XYd(double _x, double _y) : x(_x), y(_y) {}
    XYd(XY pt) : x(pt.x), y(pt.y) {}

    bool zero() const {
        return !x && !y;
    }

    double abs() const {
        return sqrt(x * x + y * y);
    }

    double abs2() const {
        return x * x + y * y;
    }

    double dot(const XYd &rhs) const {
        return x * rhs.x + y * rhs.y;
    }

    // Assuming collinear with rhs, return t st lhs = t * rhs
    double collinearDivide(const XYd &rhs) const {
        if (fabs(x) > fabs(y))
            return x / rhs.x;
        return y / rhs.y;
    }

    // Projection onto rhs
    XYd projOnto(const XYd &rhs) const {
        return this->dot(rhs) * rhs / rhs.abs2();
    }

    //X and Y are treated as increasing to bottom right
    XYd clockwise90() const {
        return XYd(-y, x);
    }

    XYd anticlockwise90() const {
        return XYd(y, -x);
    }

    // Magnitude of the cross product when treated as 3-vectors
    double orthogonalProduct(const XYd &rhs) const {
        return x * rhs.y - y * rhs.x;
    }

    XYd norm() const {
        return *this / abs();
    }

    XYd operator-() const {
        return XYd(-x, -y);
    }

    XYd operator+(const XYd &rhs) const {
        return XYd(x + rhs.x, y + rhs.y);
    }

    XYd operator+=(const XYd &rhs) {
        return (*this = *this + rhs);
    }

    XYd operator-(const XYd &rhs) const {
        return XYd(x - rhs.x, y - rhs.y);
    }

    XYd operator*(double rhs) const {
        return XYd(x * rhs, y * rhs);
    }

    XYd operator*=(double rhs) {
        return (*this = *this * rhs);
    }

    XYd operator/(double rhs) const {
        return XYd(x / rhs, y / rhs);
    }

    XYd operator/=(double rhs) {
        return (*this = *this / rhs);
    }

    operator SDL_Point() const {
        return SDL_Point{int(x), int(y)};
    }

    friend std::ostream &operator<<(std::ostream &os, const XYd &rhs);
    friend XYd operator*(double lhs, const XYd &rhs);
};

inline std::ostream &operator<<(std::ostream &os, const XYd &rhs) {
    return os << rhs.x << ", " << rhs.y;
}

inline XYd operator*(double lhs, const XYd &rhs) {
    return XYd(lhs * rhs.x, lhs * rhs.y);
}

inline XY::XY(XYd pt) : x(pt.x), y(pt.y) {}

#endif
