#include "stdio.h"
#include "math.h"
#include "object.hpp"
#include "global.hpp"
#include <vector>
#include <iostream>

using namespace std;

const double gravityConst = 4e-15;
const double distEpsConst = 1e-5;
const double velEpsConst = 1e-6;

// Mass is measured in kg
// Mass of any object marked as having 0 mass (neglible gravity)
const double zeroMassConst = 1e14;  // 100 cubic km of water

const unsigned int trailLen = 50;

// Part 1: update velocity
void Object::physicsStep1() {
    if (mass < 0.00001)
        ; //cerr << "obj " << id << " has mass " << mass << nl;
    else
        vel += gstate.stepSec * force / mass;
    vel += impulse;
    force = impulse = {0.0, 0.0};
    //cerr << "obj " << id << ": " << pos <<" vel " << vel << nl;

    //newpos = pos + gstate.stepSec * vel;
    //physicsStep2();

    stepRemaining = gstate.stepSec;
    collisions = 0;
}

// Part 2: update position
void Object::physicsStep2() {
    pos += stepRemaining * vel;

    if (collisions) {
        fprintf(stderr, "Object %d: %d collisions this step\n", id, collisions);
    }
}

void Object::updateAnimations() {
    trail.push_back(pos);
    if (trail.size() > trailLen)
        trail.pop_front();
}

void Object::collisionCheck(Object &rhs) {
    double r = radius + rhs.radius; // + distEpsConst;
    double r2 = r * r;

    // Work in a frame moving at this->vel.
    // Object 1 (v1, m1, etc.) is rhs, object 2 is lhs

    // Current offset of rhs from this object
    XYd pos1 = rhs.pos - pos;

    // v2 and pos2 is zero in this frame
    XYd v1 = rhs.vel - vel;

    if (v1.abs2() < velEpsConst)
        return;

    // rhs point of nearest approach along its line of movement
    XYd near = pos1.projOnto(v1.clockwise90());

    // Find collide point offset

    // Distance from near to collide point, squared
    double p2 = r2 - near.abs2();
    if (p2 < 0)
        return;
    XYd pos1_c = near - sqrt(p2) * v1.norm();

    double t_c = (pos1_c - pos1).collinearDivide(v1);

    //if (t_c >= -1e-5 && t_c <= gstate.stepSec) {
    // Might start intersecting, just fudge that....
    if ((t_c >= 0 || pos1.abs2() < r2) && t_c < gstate.stepSec) {

        cerr << "init dist " << pos1.abs() << nl;

        cerr << "rhs = " << rhs.pos << " pos1 = " << pos1 << "  v1 = " << v1 << nl;
        cerr << "near = " << near << " t_c = " << t_c << " timestep=" << gstate.stepSec << nl;

        if (t_c < 0) {
            t_c = 0;
            pos1_c = pos1;
            // Use fake radii to make the situation sensical
            //r2 = pos.abs2();
            //r = sqrt(r2);
        }

        cerr << "collide point: " << pos1_c << " (dist from near: " << sqrt(p2) << ")" << nl;

        // Decompose into velocity component tangent and normal to line of collision (between centres)
        XYd v1_tang = v1.projOnto(pos1_c.clockwise90());
        XYd v1_norm = v1 - v1_tang;

        cerr << "v1_tang: " << v1_tang << " v1_norm: " << v1_norm << nl;

        // coefficient of restitution (0.0=inelastic, 1.0=perfectly elastic)
        double cor = gstate.cor; //0.6;

        // Not coefficient of friction, but proportion of velocity transferred to other body
        // 0.5 is maximum (split evenly)
        double friction = gstate.friction; //0.01;

        double m1 = rhs.mass;
        if (!m1) m1 = zeroMassConst;
        double m2 = mass;
        if (!m2) m2 = zeroMassConst;

        XYd v1_norm_new = v1_norm * (m1 - cor * m2) / (m1 + m2);
        XYd v2_norm_new = v1_norm * m1 * (cor + 1) / (m1 + m2);

        cerr << "v1_norm_new: " << v1_norm_new << " v2_norm_new: " << v2_norm_new << nl;

        /*
        // Friction transfers a portion of the tangental component.
        // Not physically correct: only depends on relative masses
        double fric_portion = (friction + 1) / friction;
        v1_tang * m1
        */

        XYd v1_new = (1.0 - friction) * v1_tang + v1_norm_new;  //FIXME
        XYd v2_new = (friction * v1_tang) * m1 / m2 + v2_norm_new;

        cerr << "v1_new: " << v1_new << " v2_new: " << v2_new << nl;

        XYd pos_frame = pos;
        XYd vel_frame = vel;

        // Change position and velocity so after moving forward by velocity, rebound position is reached

        rhs.vel = v1_new + vel_frame;
        // Untranslate back to global frame of reference
        XYd pos1_c_untrans = pos_frame + pos1_c + t_c * vel_frame;
        rhs.pos = pos1_c_untrans - t_c * rhs.vel;

        XYd nextpos1 = rhs.pos + gstate.stepSec * rhs.vel;
        cerr << "new pos1 = " << (pos1_c - t_c * rhs.vel) << " ...final " << nextpos1 << " translated: " << (nextpos1 - pos_frame) << nl;

        vel = v2_new + vel_frame;
        XYd pos2_c_untrans = pos_frame + t_c * vel_frame;
        pos = pos2_c_untrans - t_c * vel;

        XYd nextpos2 = pos + gstate.stepSec * vel;
        cerr << "new pos2 = " << (-t_c * vel) << " ...final " << nextpos2 << " translated: " << (nextpos2 - pos_frame) << nl;
        cerr << "final dist = " << (nextpos1 - nextpos2).abs() << nl;

        collisions++;
        rhs.collisions++;
    }
    //if ((newpos - rhs.newpos).abs2() < minDist * minDist) {
        // Find collision time... nah
}


// Objects with nonzero mass
vector<Object *> getMasses() {
    vector<Object *> masses;
    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        if (it->mass > 0.0)
            masses.push_back(&*it);
    }
    return masses;
}

// May be called multiple times per frame
void physicsStep() {
    vector<Object *> masses = getMasses();

    if (gstate.gravityEnabled) {
        // Apply gravity
        for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
            for (auto massIt = masses.begin(); massIt != masses.end(); ++massIt) {
                const Object &mass = **massIt;
                if (&*it != *massIt) {
                    XYd diff = mass.pos - it->pos;
                    double rsum = it->radius + mass.radius;
                    // Cap gravititation attraction for close bodies to help limit chaotic behaviour due to inaccuracies
                    double dist2 = max(diff.abs2(), rsum * rsum * 2);
                    it->impulse += gstate.stepSec * mass.mass * gravityConst * diff.norm() / dist2;
                }
            }
        }
    }

    // Calc velocities
    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        it->physicsStep1();
    }

    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        for (auto it2 = incPtr(it); it2 != objectsEnd(); ++it2) {
            it->collisionCheck(*it2);
        }
    }

    // Move to final positions
    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        it->physicsStep2();
    }
}

// Call once per frame
void updateObjectAnimations() {
    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        it->updateAnimations();
    }
}

// Debugging/bookkeeping
void conservedQuantities() {
    XYd momentum;
    double energy = 0;
    double angularMomentum = 0;

    vector<Object *> masses = getMasses();

    for (auto it = objectsBegin(); it != objectsEnd(); ++it) {
        Object &obj = *it;

        momentum += obj.mass * obj.vel;
        energy += 0.5 * obj.mass * obj.vel.abs2();
        angularMomentum += obj.mass * obj.pos.orthogonalProduct(obj.vel);

        // Gravitation potential energy
        for (auto massIt = ++masses.begin(); massIt != masses.end(); ++massIt) {
            const Object &mass = **massIt;
            if (&*it != *massIt) {
                XYd diff = mass.pos - it->pos;
                energy -= obj.mass * mass.mass * gravityConst / diff.abs();
            }
        }
    }

    cerr << "system totals: energy=" << energy << " \tmomentum=" << momentum << " \tA.momentum=" << angularMomentum << nl;
    gstate.totalEnergy = energy;
}
